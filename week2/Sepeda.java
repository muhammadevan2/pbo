public class Sepeda {
        int gear = 5;
        
        public Sepeda(int jumlahRoda, String jenis, String merk){
            System.out.println("Sepeda "+jenis+" bermerk "+merk+" memiliki jumlah roda "+jumlahRoda);
        }
        void ngerem(){
            System.out.println("Sepeda direm");
        }
        
        public static void main(String args[]) {
            //membuat objek
            Sepeda sepedaAnak = new Sepeda (2,"Anak", "BMX");
            //akses atribut dan method
            int gearSepeda = sepedaAnak.gear;
            System.out.println("Jumlah gear "+gearSepeda);
            sepedaAnak.ngerem();
     
    }
}
